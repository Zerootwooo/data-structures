#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
    
    vector<string> Movies;
   
   Movies.push_back("1.) Citizen Kane");
   Movies.push_back("2.) The Godfather");
   Movies.push_back("3.) Shawshank Redemption");
   Movies.push_back("4.) Gone With The Wind");
   Movies.push_back("5.) Pulp Fiction");
   Movies.push_back("6.) Metropolis");
   Movies.push_back("7.) 12 Angry Men");
   Movies.push_back("8.) Schindler's List");
   Movies.push_back("9.) Spirited Away");
   Movies.push_back("10.) Singin' In The Rain");
   Movies.push_back("11.) The Notebook");
   Movies.push_back("12.) Star Wars: Episode V - The Empire Strikes Back");
   Movies.push_back("13.) Spider-Man: Farm From Home");
   Movies.push_back("14.) Weathering With You");
   Movies.push_back("15.) Your Name");
   Movies.push_back("16.) Lord of the Rings: Fellowship of the Ring");
   Movies.push_back("17.) Lion King");
   Movies.push_back("18.) White Christmas");
   Movies.push_back("19.) Jurassic Park");
   Movies.push_back("20.) The Dark Knight");
   Movies.push_back("21.) Matrix");
   Movies.push_back("22.) The Good, The Bad, And The Ugly");
   Movies.push_back("23.) Rear Window");
   Movies.push_back("24.) Raiders of the Lost Ark");
   Movies.push_back("25.) Journey to the Center of the Earth");
   
   int numElements = Movies.size();
   
   
  int c;
  bool valid = false;
  char choice = 'y';
  while(!valid){
  	valid = true;
  while (choice == 'y'){
  cout<<"1.) Borrow a Movie\n2.) Return or Donate a Movie\n3.) View Movie Details\n4.) Search Movie \n5.) Exit\n";
  cout<<"What would you like to do? > ";
  cin>>c;
	switch(c){
  	case 1://borrow
  	{
  		char yes='y';
  		while(yes=='y'){
  		int borrow;
  		for(int i = 0; i < Movies.size(); ++i){
       	cout << Movies[i] << "\n";
       	}
  		cout<<"What Movie would you like to borrow? > ";
  		cin>>borrow;
  		cout<<"You have successfully borrowed Movie Number "<<Movies[borrow-1]<<"!\n";
  		Movies.erase(Movies.begin() + borrow -1 );
	  	cout<<endl;
  		cout<<"Would you like to borrow another?(y/n) > ";
  		cin>>yes;
  		}
  	}
  	break;
  	case 2:
  		{
  		char yes = 'y';
  		while(yes=='y'){
  		cout<<"What Movie would you like to return or donate?\nPlease Follow the Format:\nMovieNumber.) Movie Title\n > ";
	 	string rd;
	 	cin.ignore();
		getline(cin,rd);
	 	Movies.push_back(rd);
	 	cout<<"Movie was Returned/Donated Succesfully!\n";
	 	sort(Movies.begin(),Movies.end());
	 	for(int j = 0; j < Movies.size(); ++j){
       	cout << Movies[j] << "\n";
  		}
  		cout<<"Would you like to return/donate another movie?(y/n) > ";
  		cin>>yes;
		}
  	}
  	break;
  	case 3:
  		{
  			for(int i = 0; i < Movies.size(); ++i){
       			cout << Movies[i] << "\n";
  			}
			int movnum;
			cout<<"Enter Movie Number to View Details > ";
			cin>>movnum;
			cout<<Movies[movnum-1];
			switch(movnum){
				case 1:
					cout<<"(1941)\n";
					cout<<"Director: Orson Welles\nWriters: Herman J. Mankiewicz, Orson Welles\n";
					cout<<"Starring: Orson Welles, Joseph Cotten, Dorothy Comingore\n";
				break;
				case 2:
					cout<<"(1972)\n";
					cout<<"Director: Francis Ford Coppola\n";
					cout<<"Written by: Mario Puzo, Francis Ford Coppola\n";
					cout<<"Starring: Marlon Brando, Al Pacino, James Caan\n";
				break;
				case 3:
					cout<<"(1994)\n";
					cout<<"Director: Frank Darabont\n";
					cout<<"Written by: Stephen King, Frank Darabont\n";
					cout<<"Starring: Tim Robbins, Morgan Freeman, Bob Gunton\n";
				break;
				case 4:
					cout<<"(1939)\n";
					cout<<"Directors: Victor Fleming, George Cukor\n";
					cout<<"Written by: Margaret Mitchell, Sidney Howard\n";
					cout<<"Starring: Clark Gable, Vivien Leigh, Thomas Mitchell\n";
				break;
				case 5:
					cout<<"(1994)\n";
					cout<<"Director: Quentin Tarantino\n";
					cout<<"Written by: Quentin Tarantino, Roger Avary\n";
					cout<<"Starring: John Travolta, Uma Thurman, Samuel L. Jackson\n";
				break;
				case 6:
					cout<<"(1927)\n";
					cout<<"Director: Fritz Lang\n";
					cout<<"Written by: Thea von Harbou, Thea von Harbou\n";
					cout<<"Starring: Brigitte Helm, Alfred Abel, Gustav Fröhlich\n";
				break;
				case 7:
					cout<<"(1957)\n";
					cout<<"Director: Sidney Lumet";
					cout<<"Written by: Reginald Rose, Reginald Rose\n";
					cout<<"Starring: Henry Fonda, Lee J. Cobb, Martin Balsam\n";
				break;
				case 8:
					cout<<"(1993)\n";
					cout<<"Director: Steven Spielberg\n";
					cout<<"Written by: Thomas Keneally, Steven Zaillian\n";
					cout<<"Starring: Liam Neeson, Ralph Fiennes, Ben Kingsley\n";
				break;
				case 9: 
					cout<<"(2001)\n";
					cout<<"Directors: Hayao Miyazaki, Kirk Wise\n";
					cout<<"Written by: Hayao Miyazaki\n";
					cout<<"Starring: Daveigh Chase, Suzanne Pleshette, Miyu Irino\n";
				break;
				case 10:
					cout<<"(1952)\n";
					cout<<"Directors: Stanley Donen, Gene Kelly\n";
					cout<<"Written by: Betty Comden, Adolph Green\n";
					cout<<"Starring: Gene Kelly, Donald O'Connor, Debbie Reynolds\n";
				break;
				case 11:
					cout<<"(2004)\n";
					cout<<"Director: Nick Cassavetes\n";
					cout<<"Written by: Jeremy Leven\n";
					cout<<"Starring: Ryan Gosling, Rachel McAdams\n";
				break;
				case 12:
					cout<<"(1980)\n";
					cout<<"Director: Irvin Kershner\n";
					cout<<"Written by: George Lucas\n";
					cout<<"Starring: Mark Hamill, Harrison Ford, Carrie Fisher, Billy Dee Williams, Anthony Daniels, David Prowse\n";
					
				break;
				case 13:
					cout<<"(2019)\n";
					cout<<"Director: John Watts\n";
					cout<<"Written by: Chris McKenna, Erik Sommers\n";
					cout<<"Starring: Tom Holland, Samuel L. Jackson, Zendaya, Cobie Smulders, Jake Gyllenhaal\n";
				break;
				case 14:
					cout<<"(2019)\n";
					cout<<"Director: Makoto Shinkai\n";
					cout<<"Written by: Makoto Shinkai\n";
					cout<<"Starring: Kotaro Daigo, Nana Mori";
				break;
				case 15:
					cout<<"(2016)\n";
					cout<<"Director: Makoto Shinkai\n";
					cout<<"Written by: Makoto Shinkai\n";
					cout<<"Starring: Ryunosuke Kamiki, Mone Kamishiraishi, Ryo Narita, Aoi Yuki\n";
				break;
				case 16:
					cout<<"(2001)\n";
					cout<<"Director: Peter Jackson\n";
					cout<<"Written by: J. R. R. Tolkien\n";
					cout<<"Starring: Elijah Wood, Ian McKellen, Liv Tyler, Viggo Mortensen, Orlando Bloom, Billy Boyd";
				break;
				case 17:
					cout<<"(1994)\n";
					cout<<"Directors: Roger Allers, Rob Minkoff\n";
					cout<<"Written by: Don Hahn\n";
					cout<<"Starring: Matthew Broderick, James Earl Jones, Jeremy Irons, Moira Kelly, Nathan Lane";
				break;
				case 18:
					cout<<"(1954)\n";
					cout<<"Director: Michael Curtiz\n";
					cout<<"Written by: Norman Krasna, Norman Panama, Melvin Frank";
					cout<<"Starring: Bing Crosby, Danny Kaye, Rosemary Clooney, Vera-Ellen";
				break;
				case 19:
					cout<<"(1993)\n";
					cout<<"Director: Steven Spielberg\n";
					cout<<"Written by: Michael Crichton\n";
					cout<<"Starring: Sam Neill, Laura Dern, Jeff Goldblum, Richard Attenborough, Bob Peck, Samuel L. Jackson\n";
				break;
				case 20:
					cout<<"(2008)\n";
					cout<<"Director: Christopher Nolan\n";
					cout<<"Written by: Bill Finger\n";
					cout<<"Starring: Christian Bale, Michael Caine, Heath Ledger, Gary Oldman, Morgan Freeman, Aaron Eckhart, Maggie Gyllenhaal\n";
				break;
				case 21:
					cout<<"(1999)\n";
					cout<<"Director: The Wachowskis\n";
					cout<<"Written by: The Wachowskis\n";
					cout<<"Starring: Keanu Reeves, Laurence Fishburne, Carie-Anne Moss, Hugo Weaving, Joe Pantoliano\n";
				break;
				case 22:
					cout<<"(1966)\n";
					cout<<"Director: Sergio Leone\n";
					cout<<"Written by: Age & Scarpelli, Luciano Vincenzoni\n";
					cout<<"Starring: Clint Eastwood, Eli Wallach, Lee Van Cleef\n";
				break;
				case 23:
					cout<<"(1954)\n";
					cout<<"Director: Alfred Hitchcock\n";
					cout<<"Written by: John Michael Hayes\n";
					cout<<"Starring: James Stewart, Grace Kelly, Wendell Corey, Thelma Ritter, Raymond Burr\n";
				break;
				case 24:
					cout<<"(1981)\n";
					cout<<"Director: Steven Spielberg\n";
					cout<<"Written by: Lawrence Kasdan\n";
					cout<<"Starring: Harrison Ford, Karen Allen, Paul Freeman, Ronald Lacey, John Rhys-Davies, Denholm Ellior\n";
				break;
				case 25:
					cout<<"(2008)\n";
					cout<<"Director: Eric Brevig\n";
					cout<<"Written by: Jules Vern\n";
					cout<<"Starring: Brendan Fraser, Josh Hutcherson, Anita Briem\n";
				break;
				default:
					cout<<"The details for that movie hasn't been updated yet.\n";
				break;
		}
		}
	break;
	case 4:
		{
		string mov;
		int d = Movies.size();
		cout<<"Enter a Movie > "; 
    		cin.ignore();
    		getline(cin,mov);
    		cout<<"Results for \""<<mov<<"\"\n";
    	for (int x = 0; x < d; x++){
    		if(Movies[x].find(mov, 0) != std::string::npos)
			{
    		cout << Movies[x]<< endl;
			}
			}
		}
	break;
  	default:
  		{
  		cout<<"Invalid.";
		if(cin.fail()) 
		
		{
			cin.clear(); //This corrects the stream.
			cin.ignore(); //This skips the left over stream data./
			cout << "Please enter 1 to 5 only." << endl;
			valid = false; //The cin was not an integer so try again.
		}
		}
	}
cout<<"\nWould you like to stay? (y/n) > ";
cin>>choice;
}
}
cout<<"Thank you, Come Again!";
}